const discord = require('discord.js');
const sql = require('sqlite');
const oneLinerJoke = require('one-liner-joke');
const config = require('./data/config.json');

const client = new discord.Client;
let pf = config.prefix;
sql.open('./data/steaks.db');
client.on('ready', () => { 
	console.log('Ready!')
	console.log(`Add me: https://discordapp.com/oauth2/authorize?&client_id=${client.user.id}&scope=bot&permissions=238120167`)
	client.user.setGame(`Steakhouse | ${config.prefix}help`)
	client.user.setStatus('online')
	console.error; // Gotta log those errors
	sql.run("CREATE TABLE IF NOT EXISTS scores (userId TEXT, steaks INTEGER)")
  });

function dhm(ms){
    days = Math.floor(ms / (24*60*60*1000));
    daysms = ms % (24*60*60*1000);
    hours = Math.floor((daysms)/(60*60*1000));
    hoursms = ms % (60*60*1000);
    minutes = Math.floor((hoursms)/(60*1000));
    minutesms = ms % (60*1000);
    sec = Math.floor((minutesms)/(1000));
	if (hours < 10) hours = "0" + Math.floor((daysms)/(60*60*1000)).toString()
	if (minutes < 10) minutes = "0" + Math.floor((hoursms)/(60*1000)).toString()
	if (sec < 10) sec = "0" + Math.floor((minutesms)/(1000)).toString()
    return days + "d " + hours + ":" + minutes + ":" + sec;
  }
function clean(text) {
  if (typeof(text) === 'string')
    return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
  	else
      return text;
  	} 
client.on('message', async (message) => {
	let args = message.content.split(' ')
	let command = args[0].toLowerCase()
	let mentions = message.mentions.users
	if (!message.content.startsWith(config.prefix) || message.author.bot || message.channel.type === 'dm' || message.channel.type !== 'text' || !message.channel.permissionsFor(message.guild.me).has("SEND_MESSAGES" && "EMBED_LINKS")) return;
	if (command == pf + "steak") {
		let mentioned = (mentions == 1) ? mentions.first() : null
		if (mentioned) {
			const embed = new discord.RichEmbed()
				.setDescription(':meat_on_bone: You steaked <@' + mentions.first().id + '>!')
				.setColor("#ff6b6b")
  			message.channel.send({embed});
			console.error;
			  sql.get(`SELECT * FROM score WHERE userId ='${mentions.first().id}'`).then(row => {
				if (!row) {
  					sql.run('INSERT INTO score (userId, steaks) VALUES (?, ?)', [mentions.first().id, 1]);
				} else {
  					sql.run(`UPDATE score SET steaks = ${row.steaks + 1} WHERE userId = ${mentions.first().id}`);
				}
			});
		} else { 
			const embed = new discord.RichEmbed()
				.setDescription(':meat_on_bone: **Couldnt steak** :frowning: \n There must be some error, maybe you didnt mention a user? Or you mentioned multiple?')
				.setColor("#ff6b6b")
  			message.channel.send({embed});
		}
	  }

	if (command == pf + "help") {
		const embed_h1 = new discord.RichEmbed()
			.setColor("#ff6b6b")
			.setDescription('You need help my friend? Right place to be, then! The prefix is **\"**')
			.addField('help', 'Send help. You should\'ve figured that out already. \n Usage: \'help')
			.addField('serverinfo', 'Get info about the current guild. \n Usage: \'serverinfo')
			.addField('userinfo', 'See info about an user. \n Usage: \'userinfo @User')
			.addField('info', 'Info about the bot. \n Usage: \'info')
			.addField('joke', 'Random joke. \n Usage: \'joke')
			.addField('ping', 'Test the speed of the bot. \n Usage: \'ping')
			.addField('steak', 'Steak a user. \n Usage: \'steak @User')
			.addField('invite', 'Invite me to your server! \n Usage: \'invite')
			.addField('rmsteak', 'Take a steak from a user. \n Usage: \'rmsteak @User')
			.addField('rmsteaks', 'Take all steaks from a user. \n Usage: \'rmsteaks @User')
			.addField('steaks', 'See your own or someone else\' steaks. \n Usage: \'steaks @User')
			.addField('Support', '[Join the support server!](https://discord.gg/p4BrCCX)')
  		message.author.send({embed: embed_h1})
		const embed_s = new discord.RichEmbed()
			.setColor("#ff6b6b")
			.setDescription('Check your DMs!')
  	message.channel.send({embed: embed_s});
	  }
	if (command == pf + "invite") {
  		message.channel.send("Just use this link: https://discordapp.com/oauth2/authorize?&client_id=318758679312596992&scope=bot&permissions=19456");
	  }

	if (command == pf + "ping") {
		let latency = Date.now();
		const embed  = new discord.RichEmbed()
				.setColor("#ff6b6b")
    		.addField(':ping_pong: Pinging...', 'measuring latency...', true)
  		message.channel.send({embed}).then(msg => {
   			const embed  = new discord.RichEmbed()
						.setColor("#ff6b6b")
      			.addField(':ping_pong: Pong!', Date.now() - latency + 'ms', true)
      	msg.edit({embed});
  		});
	  }

	if (command == pf + "info") {
		let count = [];
		client.guilds.forEach(g => {count.push(g.memberCount);});
		count = count.reduce((a, b) => {return a + b;});
		const embed  = new discord.RichEmbed()
			.setColor("#ff6b6b")
			.setDescription('Want some info about the bot? Here it is!', true)
    		.addField('Creator', '<@178409772436029440>', true)
			.addField('Name', '<@' + client.user.id + '>', true)
			.addField('Support', 'Insert your own info here...', true)
			.addField('ID', client.user.id, true)
			.addField('Servers', client.guilds.size, true)
			.addField('Channels', client.channels.size, true)
			.addField('Members', count, true)
			.addField('RAM Usage', `${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB`, true)
			.addField('Uptime (DD HH:MM:SS)', dhm(client.uptime), true)
			.addField('Library', '[discord.js](https://discord.js.org)', true)
			.addField('Language', '[NodeJS](https://nodejs.org/en/)', true)
			.addField('Version', '1.2EB', true)
  		message.channel.send({embed});
	  }

	if (command == pf + "serverinfo") {
		let count = [];
		client.guilds.forEach(g => {count.push(g.memberCount);});
		count = count.reduce((a, b) => {return a + b;});
		let region = (message.guild.region === "eu-central") ? "Central Europe" : (message.guild.region === "eu-west") ? "Western Europe" : (message.guild.region === "singapore") ? "Singapore" : (message.guild.region === "sydney") ? "Australia" : (message.guild.region === "brazil") ? "Brazil" : (message.guild.region === "us-west") ? "US West" : (message.guild.region === "us-east") ? "US East" : (message.guild.region === "us-south") ? "US South" : (message.guild.region === "us-central") ? "US Central" : (message.guild.region === "hongkong") ? "Hong Kong" : (message.guild.region === "russia") ? "Russia" : "Undefined"
		await message.guild.fetchMembers()
		const embed  = new discord.RichEmbed()
			.setColor("#ff6b6b")
			.setThumbnail(message.guild.iconURL)
			.setDescription('Wanna know this server\'s stats? Here they come!')
		    .addField('Server Name', message.guild.name, true)
    		.addField('Server ID', message.guild.id, true)
		    .addField('Server Owner', message.guild.owner, true)
			.addField('Owner ID', message.guild.owner.id, true)
		    .addField('Server Location', region, true)
			.addField('Created', message.guild.createdAt.toUTCString(), true)
			.addField('Channels', message.guild.channels.size, true)
			.addField('Roles', message.guild.roles.size, true)
			.addField('Online / Members ', '**Total:** ' + message.guild.members.size + '\n  **-Online:** ' + message.guild.members.filter(m=>m.user.presence.status === "online" && !m.user.bot).size + '\n  **-Idle:** '+ message.guild.members.filter(m=>m.user.presence.status === "idle" && !m.user.bot).size + '\n  **-Do not Disturb:** ' + message.guild.members.filter(m=>m.user.presence.status === "dnd" && !m.user.bot).size + '\n  **-Total Online:** ' + message.guild.members.filter(m=>m.user.presence.status === "online" || m.user.presence.status === "idle" || m.user.presence.status === "dnd" && !m.user.bot).size + '\n  **-Bot:** ' + message.guild.members.filter(m=>m.user.bot).size, false)
  		message.channel.send({embed});
	  }

	if (command == pf + "rmsteak") {
	  if (!message.channel.permissionsFor(message.author).has("MANAGE_MESSAGES") || !message.author.id == config.owner) {
		  const embed = new discord.RichEmbed()
		  	.setTitle(":meat_on_bone: You dont have Manage Messages permissions!")
		  message.channel.send({embed});
	  } else {
		if (mentions.size == 1) {
			const embed = new discord.RichEmbed()
				.setColor("#ff6b6b")
				.setDescription(':meat_on_bone: You took a steak from <@' + mentions.first().id + '>!')
			console.error;
			  sql.get(`SELECT * FROM score WHERE userId ='${mentions.first().id}'`).then(row => {
				if (!row) {
					const embed = new discord.RichEmbed()
						.setColor("#ff6b6b")
						.setDescription(':meat_on_bone: **Couldnt take steak** :frowning: \nUser has 0 steaks!')
					message.channel.send({embed});
				} else {
  					sql.run(`UPDATE score SET steaks = ${row.steaks - 1} WHERE userId = ${mentions.first().id}`);
					message.channel.send({embed});
				}
			});
		} else { 
			const embed = new discord.RichEmbed()
				.setColor("#ff6b6b")
				.setDescription(':meat_on_bone: **Couldnt take steak** :frowning: \n There must be some error, maybe you didnt mention a user? Or you mentioned multiple?')
  			message.channel.send({embed});
		}
	  }
	  }

	if (command == pf + "rmsteaks") {
	  if (!message.channel.permissionsFor(message.author).has("MANAGE_MESSAGES") || !message.author.id == config.owner) {
		const embed = new discord.RichEmbed()
		  .setTitle(":meat_on_bone: You dont have Manage Messages permissions!")
		message.channel.send({embed});
		return;
	  } else {
		if (mentions.size == 1) {
			const embed = new discord.RichEmbed()
				.setColor("#ff6b6b")
				.setDescription(':meat_on_bone: You took all steaks from <@' + mentions.first().id + '>!')
			console.error;
			  sql.get(`SELECT * FROM score WHERE userId ='${mentions.first().id}'`).then(row => {
				if (!row) {
					const embed = new discord.RichEmbed()
						.setColor("#ff6b6b")
						.setDescription(':meat_on_bone: **Couldnt take steaks** :frowning: \n That user has 0 steaks!')
  					message.channel.send({embed});
				} else {
					sql.run(`UPDATE score SET steaks = 0 WHERE userId = ${mentions.first().id}`);
					message.channel.send({embed});
				}
			});
		} else { 
			const embed = new discord.RichEmbed()
				.setColor("#ff6b6b")
				.setDescription(':meat_on_bone: **Couldnt take steaks** :frowning: \n There must be some error, maybe you didnt mention a user? Or you mentioned multiple?')
  			message.channel.send({embed});
			console.log(message.author.username + '#' + message.author.discriminator + ": rmsteaks, failed missing args in guild  \"" + message.guild.name + "\"")
		}
	  }
	  }

	if (command == pf + "steaks") {
		if (mentions.size == 1) {
				sql.get(`SELECT * FROM score WHERE userId ='${mentions.first().id}'`).then(row => {
  				if (!row) {
					const embed = new discord.RichEmbed()
						.setColor("#ff6b6b")
						.setDescription(':meat_on_bone: <@' + mentions.first().id + '> has 0 steaks!')
  					message.channel.send({embed});
					return;
				  }
				const embed = new discord.RichEmbed()
					.setTitle(':meat_on_bone: <@' + mentions.first().id + '> has ' + row.steaks + ' steaks!')
  				message.channel.send({embed});
				});
		} else { 
			if (mentions.size == 0) {
				sql.get(`SELECT * FROM score WHERE userId ='${message.author.id}'`).then(row => {
  				if (!row) {
					const embed = new discord.RichEmbed()
						.setDescription(':meat_on_bone: <@' + message.author.id + '> has 0 steaks!')
						.setColor("#ff6b6b")
  					message.channel.send({embed});
					return;
				  } else {
				const embed = new discord.RichEmbed()
					.setDescription(':meat_on_bone: <@' + message.author.id + '> has ' + row.steaks + ' steaks!')
					.setColor("#ff6b6b")
  				message.channel.send({embed});
				}});
			}
		}
		} else {
		if (mentions.size > 1) {
			const embed = new discord.RichEmbed()
				.setDescription(':meat_on_bone: **Couldnt see steaks** :frowning: \n There must be some error, maybe you mentioned multiple users?')
				.setColor("#ff6b6b")
  			message.channel.send({embed});
		}
	  }

	if (command == pf + "userinfo") {
		if (mentions.size == 1) {
			let gamestring = (mentions.first().presence.game === null) ? "None" : mentions.first().presence.game.name
			let status = (mentions.first().presence.status === "online") ? "Online" : (mentions.first().presence.status === "idle") ? "Idle" : (mentions.first().presence.status === "dnd") ? "Do not Disturb" : (mentions.first().presence.status === "offline") ? "Offline" : "Unknown"
			if (mentions.first().id == "178409772436029440") {
				const embed = new discord.RichEmbed()
					.setColor("#ff6b6b")
					.setThumbnail(mentions.first().avatarURL)
					.setDescription('Wanna know something about that guy? Here, ill tell you some details.')
					.addField('Steakhouse Creator!', 'Yes its me!', true)
					.addField('Name', mentions.first().username, true)
					.addField('Discriminator', mentions.first().discriminator, true)
					.addField('Created', mentions.first().createdAt.toUTCString(), true)
					.addField('Status', status, true)
					.addField('Game', gamestring, true)
				message.channel.send({embed})
			} else {
				const embed = new discord.RichEmbed()
					.setColor("#ff6b6b")
					.setThumbnail(mentions.first().avatarURL)
					.setDescription('Wanna know something about that guy? Here, ill tell you some details.')
					.addField('Name', mentions.first().username, true)
					.addField('Discriminator', mentions.first().discriminator, true)
					.addField('Created', mentions.first().createdAt.toUTCString(), true)
					.addField('Status', status, true)
					.addField('Game', gamestring, true)
				message.channel.send({embed})	
			}
		}
		if (mentions.size == 0) {
			let gamestring = (message.author.presence.game === null) ? "None" : message.author.presence.game.name
			let status = (message.author.presence.status === "online") ? "Online" : (message.author.presence.status === "idle") ? "Idle" : (message.author.presence.status === "dnd") ? "Do not Disturb" : (message.author.presence.status === "offline") ? "Offline" : "Unknown"
			if (message.author.id == "178409772436029440") {
				const embed = new discord.RichEmbed()
					.setColor("#ff6b6b")
					.setThumbnail(message.author.avatarURL)
					.setDescription('Wanna know something about yourself? Ill tell you what you maybe didnt know.')
					.addField('Steakhouse Creator!', 'Yes its me!', true)
					.addField('Name', message.author.username, true)
					.addField('Discriminator', message.author.discriminator, true)
					.addField('Created', message.author.createdAt.toUTCString(), true)
					.addField('Status', status, true)
					.addField('Game', gamestring, true)
				message.channel.send({embed})
			} else {
				const embed = new discord.RichEmbed()
					.setColor("#ff6b6b")
					.setThumbnail(message.author.avatarURL)
					.setDescription('Wanna know something about yourself? Ill tell you what you maybe didnt know.')
					.addField('Name', message.author.username, true)
					.addField('Discriminator', message.author.discriminator, true)
					.addField('Created', message.author.createdAt.toUTCString(), true)
					.addField('Status', status, true)
					.addField('Game', gamestring, true)
				message.channel.send({embed})
			}
		}
		if (mentions.size > 1) {
			const embed = new discord.RichEmbed()
				.setColor("#ff6b6b")
				.setDescription('**Couldnt get user info** :frowning: \n There must be some error, maybe you mentioned multiple users?')
  			message.channel.send({embed});
		}
	  }

  	if (command == pf + "eval") {
    	if (message.author.id !== config.owner) return;
  		const code = args.slice(1).join(" ");
  		try {
      		let evaled = eval(code);
			if (evaled instanceof Promise) evaled = await evaled;
			if (typeof evaled !== "string") evaled = require("util").inspect(evaled);
			evaled = evaled.replace(new RegExp(config.token, "g"), "The bot token was filtered for security reasons. ");
			eval_s = evaled.toString()
			if ( eval_s.lenght >= 2000) {
				message.channel.send(`\`\`\`xl\nResponse too long!\n\`\`\``);
			} else {
				message.channel.send(`\`\`\`xl\n${clean(evaled)}\n\`\`\``);
			}
  		}
  		catch(err) {
      		message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
  		}
  	  }

	if (command == pf + "joke") {
		const embed  = new discord.RichEmbed()
			.setDescription(oneLinerJoke.getRandomJoke().body)
  		message.channel.send({embed});
	  }

	if (command == pf + "say") {
		if (message.author.id == config.owner && message.channel.permissionsFor(client.user).has("ADMINISTRATOR") || message.author.id == message.guild.owner.id) {
			const text = args.slice(1).join(" ");
  			message.channel.send(text)
			message.delete()
		} else {
			const embed  = new discord.RichEmbed()
				.setColor("#ff6b6b")
				.setTitle("You dont have Administrator perms!")
				.setDescription("Or the bot is missing some perms. Please reinvite it, you can use 'invite to get the new invite.")
  			message.channel.send({embed});
		}
	  }
  });
client.login(config.token)